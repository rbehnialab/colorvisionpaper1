"""
a bunch of nonlinear functions
"""

import numpy as np


def tanh(x, r0=0, a=1):

    x = np.tanh(x - r0) + np.tanh(r0)

    return a * x


def relu(x, r0=0, a=1):

    x = np.asarray(x)
    xbool = x < r0
    x[xbool] = float(r0)

    x = float(a) * x

    return x


def tanh_like(x, r0, a=1):
    """tanh-like function from Rajan, Abbott, Sompolinsky (2010).
    """
    x = np.asarray(x)
    y = x.copy()

    y[x<=0] = (1 - r0) * np.tanh(x[x<=0]/(1 - r0))
    y[x>0] = (1 + r0) * np.tanh(x[x>0]/(1 + r0))

    return a * y


def inverse_tanh_like(x, r0, a=1):
    """tanh-like function from Rajan, Abbott, Sompolinsky (2010).
    """
    x = np.asarray(x)
    y = x.copy()

    y[x<=0] = np.arctanh(x[x<=0]/(1 - r0)) * (1 - r0)
    y[x>0] = np.arctanh(x[x>0]/(1 + r0)) * (1 + r0)

    return y / a


def tanh_like1(x, r0):
    """tanh-like function from Rajan, Abbott, Sompolinsky (2010).
    """
    assert np.all(r0 > -1)

    x = np.asarray(x)
    y = x.copy()

    y[x<=0] = (1 - r0) * np.tanh(x[x<=0]/(1 - r0))
    y[x>0] = (1 + r0) * np.tanh(x[x>0]/(1 + r0))

    a = 1/(r0 + 1)

    return a * y


def inverse_tanh_like1(x, r0):
    """tanh-like function from Rajan, Abbott, Sompolinsky (2010).
    """
    assert np.all(r0 > -1)

    x = np.asarray(x)
    y = x.copy()

    y[x<=0] = np.arctanh(x[x<=0]/(1 - r0)) * (1 - r0)
    y[x>0] = np.arctanh(x[x>0]/(1 + r0)) * (1 + r0)

    a = 1/(r0 + 1)

    return y / a


def linear_saturation(x, r0, a=1):
    """linear saturation function
    """
    def func(x):
        x = np.asarray(x)
        y = x.copy()

        y[x > 1] = 1
        y[x < -1] = -1

        return y

    return a * func(x - r0) - func(-r0)
