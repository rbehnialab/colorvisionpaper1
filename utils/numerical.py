"""
Useful functions for numerical operations
"""

import numpy as np
from scipy.interpolate import interp1d
from scipy.ndimage import gaussian_filter
from itertools import product

around = np.vectorize(np.round)

def binary_threshold(arr, thresh):
    """return arr thresholded and as integer
    """
    return (arr > thresh).astype(int)

def interp_resolve(arr, resolve=2, axis=-1, **kwargs):
    """Extent resolution via interpolation
    """
    old_range = np.arange(arr.shape[axis])
    new_range = np.arange(arr.shape[axis]*resolve)/resolve
    return interp1d(old_range, arr, axis=axis, **kwargs)(new_range)

def interp_to_finites(arr, axis=-1, bounds_error=False):
    """interpolate nans and infinites in an array.
    """
    #TODO if one axis has only nans
    other_axes = tuple(dim for dim in range(arr.ndim) if (dim != axis) and ((dim-arr.ndim) != axis))
    other_shape = tuple(shape for dim, shape in enumerate(arr.shape) if dim in other_axes)

    arange = np.arange(arr.shape[axis])
    finites = np.isfinite(arr)

    finite_axis = finites.all(other_axes)
    arr_finites = arr.compress(finite_axis, axis)

    #find first and last values along correct axes
    finite_idxs = np.argwhere(finites)

    _, first_finite_idxs = np.unique(finite_idxs[:, other_axes], axis=0, return_index=True)
    first_values = arr[tuple(finite_idxs[first_finite_idxs].T)].reshape(*other_shape)

    _, last_finite_idxs = np.unique(finite_idxs[::-1, other_axes], axis=0, return_index=True)
    last_values = arr[tuple(finite_idxs[::-1][last_finite_idxs].T)].reshape(*other_shape)

    fill_value = (first_values, last_values)

    #interpolate nans and infinites for bounds fill first and last values
    interp_arr = interp1d(
            arange[finite_axis], arr_finites,
            axis=axis, bounds_error=bounds_error,
            fill_value=fill_value
            )(arange)

    interp_arr[finites] = arr[finites]
    return interp_arr

def round(x, method=None, **kwargs):
    """round given specific method
    """
    if not np.all(np.isfinite(x)):
        raise ValueError('Only float and int types for rounding')

    digits_to_decimals = lambda x, digits: -np.floor(np.log10(np.abs(x))) + digits - 1

    if method is None:
        return np.round(x, **kwargs)
    elif method == 'odd':
        return np.floor(x) + (x % 2 < 1)
    elif method == 'even':
        return np.floor(x) + (x % 2 >= 1)
    elif method == 'evendown':
        return np.floor(x-1) + ((x - 1) % 2 >= 1)
    elif method in ['sig', 'sigdown', 'sigup']:
        digits = kwargs.get('digits', 1)
        if isinstance(x, (float, int)):
            if x == 0:
                return x
            else:
                decimals = int(digits_to_decimals(x, digits))
                if method == 'sig':
                    return np.round(x, decimals)
                elif method == 'sigdown':
                    return np.floor(x*10**decimals) / (10**decimals)
                elif method == 'sigup':
                    return np.ceil(x*10**decimals) / (10**decimals)
        else:
            x = np.nan_to_num(x)
            decimals = digits_to_decimals(x[x != 0], digits).astype(int)
            if method == 'sig':
                x[x != 0] = around(x[x != 0], decimals)
            elif method == 'sigdown':
                x[x != 0] = np.floor(x[x != 0]*10**decimals) /(10**decimals)
            elif method == 'sigup':
                x[x != 0] = np.ceil(x[x != 0]*10**decimals) /(10**decimals)
            return x
    elif method == 'base':
        base = kwargs.get('base', 5.)
        return np.round(x/base, 0) * base
    elif method == 'down':
        decimals = kwargs.get('decimals', 0)
        return np.floor(x*10**decimals) / (10**decimals)
    elif method == 'up':
        decimals = kwargs.get('decimals', 0)
        return np.ceil(x*10**decimals) / (10**decimals)
    else:
        raise NameError(f"rounding method {method} does not exist")

def argclosest(arr, value):
    """return argument/s of arr closes to value/s
    """
    if isinstance(value, (float, int)):
        return np.nanargmin(np.abs(arr - value))
    else:
        #ensure it is a numpy array - must be 1 Dimensional
        value = np.array(value)[None, :]
        #reshape arr appropriately
        shape = arr.shape
        reshape = np.multiply.reduce(shape)
        arr = arr.reshape(reshape)[:, None]
        #
        return np.nanargmin(np.abs(arr-value), axis=0)

def fwhm(data, times, axis=None, method='nanargmax'):
    """Takes one revolution of a simple wave and calculates
    its FWHM (phase-independent).
    """
    if axis is None:
        argmax = getattr(np, method)(data)
        amplitude = np.nanmax(data) - np.nanmin(data)
        halfmax = np.nanmin(data) + amplitude/2.
        #
        roller = len(data)//2 - argmax
        #
        data = np.roll(data, roller, axis=axis)
        #
        arg1, arg2 = np.where(data >= halfmax)[0][[0, -1]]
        #
        return times[arg2] - times[arg1]
    else:
        return np.apply_along_axis(fwhm, axis, data, times)

def calculate_snr(data, t, noise_interval, signal_interval, ddof=1):
    """Calculate SNR from variance of a noise interval and signal interval
    """
    if isinstance(noise_interval, tuple):
        noise_interval = [noise_interval]
    if isinstance(signal_interval, tuple):
        signal_interval = [signal_interval]
    noise_bool = np.logical_or.reduce(
        [np.logical_and(t >= start, t < end) for start, end in noise_interval])
    signal_bool = np.logical_or.reduce(
        [np.logical_and(t >= start, t < end) for start, end in signal_interval])
    noise = data[noise_bool]
    signal = data[signal_bool]
    snr = np.abs(
        np.nanvar(signal, 0, ddof=ddof)/np.nanvar(noise, 0, ddof=ddof)
    )
    return snr

def amplitude(
        data, t, axis=None,
        measurement='diff', method='nanmean',
        a_method=None, b_method=None,
        a_interval=None, b_interval=None,
        a_kwargs=None, b_kwargs=None,
        **kwargs
        ):
    """calculate the amplitude given intervals and a method
    """
    ###determine the method and arguments for baseline and amplitude calculation
    if a_method is None:
        a_method = method
    if b_method is None:
        b_method = method
    if a_kwargs is None:
        a_kwargs = kwargs
    if b_kwargs is None:
        b_kwargs = kwargs
    if isinstance(a_method, str):
        if 'abs' in a_method:
            a_func = getattr(np, a_method.replace('abs', ''))

            def a_method(x, **kwargs):
                return a_func(np.abs(x), **kwargs)

        else:
            a_method = getattr(np, a_method)
    if isinstance(b_method, str):
        if 'abs' in b_method:
            b_func = getattr(np, b_method.replace('abs', ''))

            def b_method(x, **kwargs):
                return b_func(np.abs(x), **kwargs)

        else:
            b_method = getattr(np, b_method)
    ###calculate amplitude
    if a_interval is None:
        amp = np.nanmax(data, axis=axis)
    elif a_interval == 'max':
        amp = np.nanmax(data, axis=axis)
    elif isinstance(a_interval, (int, float)):
        tbool = t >= a_interval
        amp = a_method(data[tbool], axis=axis, **a_kwargs)
    elif isinstance(a_interval, tuple):
        tbool = (t >= a_interval[0]) & (t < a_interval[1])
        amp = a_method(data[tbool], axis=axis, **a_kwargs)
    else:

        amps = []

        if not isinstance(a_method, list):
            a_method = [a_method] * len(a_interval)
        else:
            assert len(a_method) == len(a_interval), (
                    'list-like methods and '
                    'intervals must be same length')
        if not isinstance(a_kwargs, list):
            a_kwargs = [a_kwargs] * len(a_interval)
        else:
            assert len(a_kwargs) == len(a_interval), (
                    'list-like kwargs and '
                    'intervals must be same length'
                    )

        if not isinstance(b_interval, list):
            b_interval = [b_interval] * len(a_interval)
        else:
            assert len(b_interval) == len(a_interval)

        if not isinstance(b_method, list):
            b_method = [b_method] * len(a_interval)
        else:
            assert len(b_method) == len(a_interval)

        if not isinstance(b_kwargs, list):
            b_kwargs = [b_kwargs] * len(a_interval)
        else:
            assert len(b_kwargs) == len(a_interval)

        for ia_interval, ia_method, ia_kwargs, ib_interval, \
                ib_method, ib_kwargs \
                in zip(a_interval, a_method, a_kwargs, b_interval, b_method, b_kwargs):

            amp = amplitude(
                    data, t, axis=axis,
                    measurement=measurement, method=method,
                    a_method=ia_method, b_method=ib_method,
                    a_interval=ia_interval, b_interval=ib_interval,
                    a_kwargs=ia_kwargs, b_kwargs=ib_kwargs,
                    **kwargs
                    )
            amps.append(amp)

        return np.asarray(amps)

    ###calculate the baseline
    if b_interval is None:
        baseline = 0
    elif b_interval == 'min':
        baseline = np.nanmin(data, axis=axis)
    elif isinstance(b_interval, (int, float)):
        tbool = t < b_interval
        baseline = b_method(data[tbool], axis=axis, **b_kwargs)
    elif isinstance(b_interval, (tuple)):
        tbool = (t >= b_interval[0]) & (t < b_interval[1])
        baseline = b_method(data[tbool], axis=axis, **b_kwargs)
    else:
        raise TypeError(f'b_interval {type(b_interval)}')
    # TODO implement multiple baseline for amplitude calculation
    ###
    if measurement == 'diff':
        return amp - baseline
    elif measurement == 'foldchange':
        return (amp - baseline)/baseline
    elif measurement == 'ratio':
        return amp/baseline
    else:
        raise NameError(f'measurement {measurement} does not exist as an option')

def unique(arr, axis=None, use_numpy=False):
    """return set of unique entries along axis

    Parameters
    ----------
    arr : numpy.array
        array to find unique entries in
    axis : int
        axis along which to find unique entries in
    use_numpy : boolean
        If True, use numpy.unique function

    """
    #TODO dealing with nans
    if not isinstance(arr, np.ndarray):
        arr = np.array(arr)
    if use_numpy:
        try:
            return np.unique(arr, axis=axis)
        except TypeError:
            return np.unique(arr.astype('U'), axis=axis)
    elif axis is None:
        return set(arr.reshape(-1))
    else:
        return set(map(tuplefy, np.moveaxis(arr, axis, 0)))

def tuplefy(x):
    """return tuple of x if iterable and not string
    """
    if isinstance(x, str) or not hasattr(x, '__iter__'):
        return x
    else:
        return tuple(x)

def scale(arr, axis=None, has_nan=False, bounds=None):
    """scale array to be between 0 and 1
    """
    try:
        if has_nan:
            minimum = np.nanmin(arr, axis=axis, keepdims=True)
            maximum = np.nanmax(arr, axis=axis, keepdims=True)
        else:
            minimum = np.min(arr, axis=axis, keepdims=True)
            maximum = np.max(arr, axis=axis, keepdims=True)
    except TypeError:
        if has_nan:
            minimum = np.nanmin(arr, axis=axis)
            maximum = np.nanmax(arr, axis=axis)
        else:
            minimum = np.min(arr, axis=axis)
            maximum = np.max(arr, axis=axis)

    #scale array
    scaled_arr = (arr - minimum)/(maximum - minimum)
    if bounds is None:
        return scaled_arr
    elif isinstance(bounds, (float, int)):
        return scaled_arr * bounds
    else:
        return scaled_arr * (bounds[1] - bounds[0]) + bounds[0]

def nonzero_mean(arr, axis=None, has_nan=False):
    """Calculate the nonzero mean of an array
    """
    if has_nan:
        n = np.nansum(arr != 0, axis=axis)
        arr_sum = np.nansum(arr, axis=axis)
    else:
        n = np.sum(arr != 0, axis=axis)
        arr_sum = np.sum(arr, axis=axis)
    return np.true_divide(arr_sum, n)


def sliding_window(data, size, stepsize=1, padded=False, axis=-1, copy=True):
    """
    Calculate a sliding window over a signal

    Parameters
    ----------
    data : numpy array
        The array to be slided over.
    size : int
        The sliding window size
    stepsize : int
        The sliding window stepsize. Defaults to 1.
    axis : int
        The axis to slide over. Defaults to the last axis.
    copy : bool
        Return strided array as copy to avoid sideffects when manipulating the
        output array.


    Returns
    -------
    data : numpy array
        A matrix where row in last dimension consists of one instance
        of the sliding window.

    Notes
    -----

    - Be wary of setting `copy` to `False` as undesired sideffects with the
      output values may occurr.

    Examples
    --------

    >>> a = numpy.array([1, 2, 3, 4, 5])
    >>> sliding_window(a, size=3)
    array([[1, 2, 3],
           [2, 3, 4],
           [3, 4, 5]])
    >>> sliding_window(a, size=3, stepsize=2)
    array([[1, 2, 3],
           [3, 4, 5]])

    See Also
    --------
    pieces : Calculate number of pieces available by sliding

    """
    if axis >= data.ndim:
        raise ValueError(
            "Axis value out of range"
        )

    if stepsize < 1:
        raise ValueError(
            "Stepsize may not be zero or negative"
        )

    if size > data.shape[axis]:
        raise ValueError(
            "Sliding window size may not exceed size of selected axis"
        )

    shape = list(data.shape)
    shape[axis] = numpy.floor(data.shape[axis] / stepsize - size / stepsize + 1).astype(int)
    shape.append(size)

    strides = list(data.strides)
    strides[axis] *= stepsize
    strides.append(data.strides[axis])

    strided = numpy.lib.stride_tricks.as_strided(
        data, shape=shape, strides=strides
    )

    if copy:
        return strided.copy()
    else:
        return strided


def simple_rolling_window(
        arr, window, steps=1, axis=None, extend_with_nans=False,
        border=None
        ):
    """Compute rolling window given array and window
    """
    if axis is None:
        if isinstance(border, tuple):
            arr_extended = np.concatenate(
                (border[0]*np.ones(int(np.floor(window/2))),
                 arr, border[1]*np.ones(int(np.ceil(window/2)-1))))
        elif isinstance(border, (float, int)):
            arr_extended = np.concatenate(
                (border*np.ones(int(np.floor(window/2))),
                 arr, border*np.ones(int(np.ceil(window/2)-1))))
        elif border == 'edges':
            arr_extended = np.concatenate(
                (arr[0]*np.ones(int(np.floor(window/2))),
                 arr, arr[-1]*np.ones(int(np.ceil(window/2)-1))))
        elif extend_with_nans or (border in ['nans', 'nan']):
            arr_extended = np.concatenate(
                (np.nan*np.ones(int(np.floor(window/2))),
                 arr, np.nan*np.ones(int(np.ceil(window/2)-1))))
        else:
            arr_extended = arr[(arr.shape[-1] % window):]

        shape = arr_extended.shape[:-1] + \
            (arr_extended.shape[-1] - window + 1, window)
        strides = arr_extended.strides + (arr_extended.strides[-1],)

        return np.lib.stride_tricks.as_strided(
            arr_extended, shape=shape, strides=strides)[::steps]
    else:
        return np.apply_along_axis(
                simple_rolling_window, axis, arr,
                border=border,
                window=window, steps=steps, extend_with_nans=extend_with_nans)


def rolling_window(array, window=(0,), asteps=None, wsteps=None, axes=None, toend=True):
    """Create a view of `array` which for every point gives the n-dimensional
    neighbourhood of size window. New dimensions are added at the end of
    `array` or after the corresponding original dimension.

    Parameters
    ----------
    array : array_like
        Array to which the rolling window is applied.
    window : int or tuple
        Either a single integer to create a window of only the last axis or a
        tuple to create it for the last len(window) axes. 0 can be used as a
        to ignore a dimension in the window.
    asteps : tuple
        Aligned at the last axis, new steps for the original array, ie. for
        creation of non-overlapping windows. (Equivalent to slicing result)
    wsteps : int or tuple (same size as window)
        steps for the added window dimensions. These can be 0 to repeat values
        along the axis.
    axes: int or tuple
        If given, must have the same size as window. In this case window is
        interpreted as the size in the dimension given by axes. IE. a window
        of (2, 1) is equivalent to window=2 and axis=-2.
    toend : bool
        If False, the new dimensions are right after the corresponding original
        dimension, instead of at the end of the array. Adding the new axes at the
        end makes it easier to get the neighborhood, however toend=False will give
        a more intuitive result if you view the whole array.

    Returns
    -------
    A view on `array` which is smaller to fit the windows and has windows added
    dimensions (0s not counting), ie. every point of `array` is an array of size
    window.

    Examples
    --------
    >>> a = np.arange(9).reshape(3,3)
    >>> rolling_window(a, (2,2))
    array([[[[0, 1],
             [3, 4]],
            [[1, 2],
             [4, 5]]],
           [[[3, 4],
             [6, 7]],
            [[4, 5],
             [7, 8]]]])

    Or to create non-overlapping windows, but only along the first dimension:
    >>> rolling_window(a, (2,0), asteps=(2,1))
    array([[[0, 3],
            [1, 4],
            [2, 5]]])

    Note that the 0 is discared, so that the output dimension is 3:
    >>> rolling_window(a, (2,0), asteps=(2,1)).shape
    (1, 3, 2)

    This is useful for example to calculate the maximum in all (overlapping)
    2x2 submatrixes:
    >>> rolling_window(a, (2,2)).max((2,3))
    array([[4, 5],
           [7, 8]])

    Or delay embedding (3D embedding with delay 2):
    >>> x = np.arange(10)
    >>> rolling_window(x, 3, wsteps=2)
    array([[0, 2, 4],
           [1, 3, 5],
           [2, 4, 6],
           [3, 5, 7],
           [4, 6, 8],
           [5, 7, 9]])
    """
    array = np.asarray(array)
    orig_shape = np.asarray(array.shape)
    window = np.atleast_1d(window).astype(int) # maybe crude to cast to int...

    if axes is not None:
        axes = np.atleast_1d(axes)
        w = np.zeros(array.ndim, dtype=int)
        for axis, size in zip(axes, window):
            w[axis] = size
        window = w

    # Check if window is legal:
    if window.ndim > 1:
        raise ValueError("`window` must be one-dimensional.")
    if np.any(window < 0):
        raise ValueError("All elements of `window` must be larger then 1.")
    if len(array.shape) < len(window):
        raise ValueError("`window` length must be less or equal `array` dimension.")

    _asteps = np.ones_like(orig_shape)
    if asteps is not None:
        asteps = np.atleast_1d(asteps)
        if asteps.ndim != 1:
            raise ValueError("`asteps` must be either a scalar or one dimensional.")
        if len(asteps) > array.ndim:
            raise ValueError("`asteps` cannot be longer then the `array` dimension.")
        # does not enforce alignment, so that steps can be same as window too.
        _asteps[-len(asteps):] = asteps

        if np.any(asteps < 1):
             raise ValueError("All elements of `asteps` must be larger then 1.")
    asteps = _asteps

    _wsteps = np.ones_like(window)
    if wsteps is not None:
        wsteps = np.atleast_1d(wsteps)
        if wsteps.shape != window.shape:
            raise ValueError("`wsteps` must have the same shape as `window`.")
        if np.any(wsteps < 0):
             raise ValueError("All elements of `wsteps` must be larger then 0.")

        _wsteps[:] = wsteps
        _wsteps[window == 0] = 1 # make sure that steps are 1 for non-existing dims.
    wsteps = _wsteps

    # Check that the window would not be larger then the original:
    if np.any(orig_shape[-len(window):] < window * wsteps):
        raise ValueError("`window` * `wsteps` larger then `array` in at least one dimension.")

    new_shape = orig_shape # just renaming...

    # For calculating the new shape 0s must act like 1s:
    _window = window.copy()
    _window[_window==0] = 1

    new_shape[-len(window):] += wsteps - _window * wsteps
    new_shape = (new_shape + asteps - 1) // asteps
    # make sure the new_shape is at least 1 in any "old" dimension (ie. steps
    # is (too) large, but we do not care.
    new_shape[new_shape < 1] = 1
    shape = new_shape

    strides = np.asarray(array.strides)
    strides *= asteps
    new_strides = array.strides[-len(window):] * wsteps

    # The full new shape and strides:
    if toend:
        new_shape = np.concatenate((shape, window))
        new_strides = np.concatenate((strides, new_strides))
    else:
        _ = np.zeros_like(shape)
        _[-len(window):] = window
        _window = _.copy()
        _[-len(window):] = new_strides
        _new_strides = _

        new_shape = np.zeros(len(shape)*2, dtype=int)
        new_strides = np.zeros(len(shape)*2, dtype=int)

        new_shape[::2] = shape
        new_strides[::2] = strides
        new_shape[1::2] = _window
        new_strides[1::2] = _new_strides

    new_strides = new_strides[new_shape != 0]
    new_shape = new_shape[new_shape != 0]

    return np.lib.stride_tricks.as_strided(array, shape=new_shape, strides=new_strides)


def nangaussian_filter(arr, *args, return_nans=False, **kwargs):
    """
    Uses scipy.ndimage.gaussian_filter and takes care of nans
    """
    num = arr.copy()
    num[np.isnan(arr)] = 0
    num = gaussian_filter(num, *args, **kwargs)

    denom = np.ones(num.shape)
    denom[np.isnan(arr)] = 0
    denom = gaussian_filter(denom, *args, **kwargs)

    filtered = num/denom
    if return_nans:
        filtered[np.isnan(arr)] = np.nan

    return filtered


def bin_limits(binarr):
    return np.nanmin(binarr), np.nanmax(binarr)


def bins_linspace(binnumber=10, binlimits=None, binarr=None):
    """Return array with binlimits and x+1 number of bins
    binlimits inclusive
    """
    if binlimits is None:
        binlimits = bin_limits(binarr)
    #make sure edges are dealt with correctly
    arr_linspace = np.linspace(
        binlimits[0], np.nextafter(
            binlimits[-1],
            binlimits[-1]+1),
        binnumber+1)
    return arr_linspace


def bins_logspace(binnumber=10, binlimits=None, binarr=None):
    """Return array with binlimits and x+1 number of bins
    binlimits inclusive
    """
    if binlimits is None:
        binlimits = bin_limits(binarr)
    #make sure edges are dealt with correctly
    arr_logspace = np.logspace(
        np.log10(binlimits[0]), np.log10(np.nextafter(
            binlimits[-1],
            binlimits[-1]+1)),
        binnumber+1)
    return arr_logspace


def bins_arange(binsize=1, binlimits=None, binarr=None):
    """Return array with binlimits and binsize x
    binlimits inclusive
    """
    if binlimits is None:
        binlimits = bin_limits(binarr)
    #make sure edges are dealt with correctly
    arr_arange = np.arange(
        binlimits[0], np.nextafter(
            binlimits[-1]+binsize,
            binlimits[-1]+binsize+1),
        binsize)
    return arr_arange


def binarr_linspace(binarr, binnumber=10, binlimits=None, return_bool=False):
    """Return idx array sorted into x number of bins
    and with a given bin limits
    """
    bins = bins_linspace(binnumber, binlimits, binarr)
    if return_bool:
        return np.array(
            [(binarr >= binstart) & (binarr < binend)
             for binstart, binend in zip(bins[:-1], bins[1:])]
            )
    return np.array(
        [np.where((binarr >= binstart) & (binarr < binend))[0]
         for binstart, binend in zip(bins[:-1], bins[1:])]
        )


def binarr_logspace(binarr, binnumber=10, binlimits=None, return_bool=False):
    """Return idx array sorted into x number of bins
    and with a given bin limits
    """
    bins = bins_logspace(binnumber, binlimits, binarr)
    if return_bool:
        return np.array(
            [(binarr >= binstart) & (binarr < binend)
             for binstart, binend in zip(bins[:-1], bins[1:])]
            )
    return np.array(
        [np.where((binarr >= binstart) & (binarr < binend))[0]
         for binstart, binend in zip(bins[:-1], bins[1:])]
        )


def binarr_arange(binarr, binsize=1, binlimits=None, return_bool=False):
    """Return idx array sorted into bins with size x
    and with a given bin limits
    """
    bins = bins_arange(binsize, binlimits, binarr)
    if return_bool:
        return np.array(
            [(binarr >= binstart) & (binarr < binend)
             for binstart, binend in zip(bins[:-1], bins[1:])]
            )
    return np.array(
        [np.where((binarr >= binstart) & (binarr < binend))[0]
         for binstart, binend in zip(bins[:-1], bins[1:])]
        )


def multibinarr(
    binsizes, binlimits=None,
    binfunctions=binarr_arange, return_bool=False, *binarrs, **kwargs
    ):
    """same as binarr_arange/linspace just over multiple types of arrays
    """
    binarrs = list(binarrs) + list(kwargs.values())
    if not isinstance(binsizes, list):
        binsizes = [binsizes] * len(binarrs)
    if binlimits is None:
        binlimits = [None] * len(binarrs)
    if not isinstance(binfunctions, list):
        binfunctions = [binfunctions] * len(binarrs)
    boolarrs = []
    for iarr, ibin, ilimits, ifunction in zip(
        binarrs, binsizes, binlimits, binfunctions):

        if isinstance(ifunction, (str, unicode)):
            if ifunction == 'linspace':
                ifunction = binarr_linspace
            elif ifunction == 'arange':
                ifunction = binarr_arange
            else:
                raise TypeError('function is not arange or linspace')
        boolarrs += [ifunction(iarr, ibin, ilimits, True)]
    bools = []
    for boolarr in product(*boolarrs):
        bools += [np.logical_and.reduce(boolarr)]
    if return_bool:
        return np.array(bools)
    else:
        return np.array([np.where(ibool)[0] for ibool in bools])


def binned_arange_statistic(
        data, xdata, statistic=np.nanmean,
        binsize=1, binlimits=None, **kwargs):
    """Return statistic of data mapped onto xdata with
    binsize x and binlimits.
    """
    idx = binarr_arange(xdata, binsize, binlimits)
    # problems with empty arrays for max and min
    return np.array([statistic(data[iidx], **kwargs)
                     if len(iidx) > 0 else np.nan
                     for iidx in idx])


def binned_linspace_statistic(
        data, xdata, statistic=np.nanmean,
        binnumber=10, binlimits=None, **kwargs):
    """Return statistic of data mapped onto xdata with
    x number of bins and binlimits.
    """
    idx = binarr_linspace(xdata, binnumber, binlimits)
    # problems with empty arrays for max and min
    return np.array([statistic(data[iidx], **kwargs)
                     if len(iidx) > 0 else np.nan
                     for iidx in idx])


def binned_logspace_statistic(
        data, xdata, statistic=np.nanmean,
        binnumber=10, binlimits=None, **kwargs):
    """Return statistic of data mapped onto xdata with
    x number of bins and binlimits.
    """
    idx = binarr_logspace(xdata, binnumber, binlimits)
    # problems with empty arrays for max and min
    return np.array([statistic(data[iidx], **kwargs)
                     if len(iidx) > 0 else np.nan
                     for iidx in idx])


def binned_statistic2D(
        data, xdata, ydata, statistic=np.nanmean,
        xbin_func='linspace', ybin_func='linspace',
        xbinnumber=10, xbinlimits=None,
        ybinnumber=10, ybinlimits=None,
        **kwargs
        ):
    """Return statistic of data mapped to xdata and ydata
    """
    if xbin_func == 'linspace':
        xbin_func = binarr_linspace
    elif xbin_func == 'arange':
        xbin_func = binarr_arange
    elif xbin_func == 'logspace':
        xbin_func = binarr_logspace
    else:
        raise NameError(f'binning function {xbin_func} does not exist')

    if ybin_func == 'linspace':
        ybin_func = binarr_linspace
    elif ybin_func == 'arange':
        ybin_func = binarr_arange
    elif ybin_func == 'logspace':
        ybin_func = binarr_logspace
    else:
        raise NameError(f'binning function {ybin_func} does not exist')

    xidx = xbin_func(xdata, xbinnumber, xbinlimits)
    yidx = ybin_func(ydata, ybinnumber, ybinlimits)

    stat = np.array([
        [
            statistic(data[list(set(xiidx) & set(yiidx))], **kwargs)
            if len(set(xiidx) & set(yiidx)) > 0 else np.nan
            for xiidx in xidx
        ]
        for yiidx in yidx])

    return stat


def binned_statistic3D(
        data, xdata, ydata, zdata, statistic=np.nanmean,
        xbin_func='linspace', ybin_func='linspace',
        zbin_func='linspace',
        xbinnumber=10, xbinlimits=None,
        ybinnumber=10, ybinlimits=None,
        zbinnumber=10, zbinlimits=None,
        **kwargs
        ):
    """Return statistic of data mapped to xdata and ydata
    """
    data = np.asarray(data)
    xdata = np.asarray(xdata)
    ydata = np.asarray(ydata)
    zdata = np.asarray(zdata)

    if xbin_func == 'linspace':
        xbin_func = binarr_linspace
    elif xbin_func == 'arange':
        xbin_func = binarr_arange
    elif xbin_func == 'logspace':
        xbin_func = binarr_logspace
    else:
        raise NameError(f'binning function {xbin_func} does not exist')

    if ybin_func == 'linspace':
        ybin_func = binarr_linspace
    elif ybin_func == 'arange':
        ybin_func = binarr_arange
    elif ybin_func == 'logspace':
        ybin_func = binarr_logspace
    else:
        raise NameError(f'binning function {ybin_func} does not exist')

    if zbin_func == 'linspace':
        zbin_func = binarr_linspace
    elif zbin_func == 'arange':
        zbin_func = binarr_arange
    elif zbin_func == 'logspace':
        zbin_func = binarr_logspace
    else:
        raise NameError(f'binning function {ybin_func} does not exist')

    xidx = xbin_func(xdata, xbinnumber, xbinlimits)
    yidx = ybin_func(ydata, ybinnumber, ybinlimits)
    zidx = zbin_func(zdata, zbinnumber, zbinlimits)

    stat = np.array([
        [
            [
                statistic(
                    data[list(set(xiidx) & set(yiidx) & set(ziidx))],
                    **kwargs)
                if len(set(xiidx) & set(yiidx) & set(ziidx)) > 0 else np.nan
                for xiidx in xidx
            ]
            for yiidx in yidx
        ]
        for ziidx in zidx
    ])

    return stat


def logical_and2D(*boolarrs):
    """given a list of 2D boolean arrays it will return a 2D boolean array
    with the columns the length of the arrays (axis=-1) and the number of
    rows equal to len(product(*boolarrs))
    """
    bools = [np.logical_and.reduce(boolarr)
             for boolarr in product(*boolarrs)]
    return np.array(bools)


def logical_or2D(*boolarrs):
    """given a list of 2D boolean arrays it will return a 2D boolean array
    with the columns the length of the arrays (axis=-1) and the number of
    rows equal to len(product(*boolarrs))
    """
    bools = [np.logical_or.reduce(boolarr)
             for boolarr in product(*boolarrs)]
    return np.array(bools)


def logical_or(*boolarrs):
    return np.logical_or.reduce(boolarrs)


def logical_and(*boolarrs):
    return np.logical_and.reduce(boolarrs)


def idx2d_to_arr(arr, idx):
    """Return 2D array of arr given 2D array of idx (including object)
    """
    return np.array([arr[iidx] for iidx in idx])


def zscore(arr, ddof=0):
    """Return zscore for arr
    """
    return (arr - np.nanmean(arr))/np.nanstd(arr, ddof=ddof)


def normabssum(arr):
    """
    """

    return arr/np.nansum(np.abs(arr))


def normint(arr):
    """Return normalized arr with the sum equal to 1
    """
    return (arr - np.nanmean(arr)) \
        / (np.sqrt(np.nansum((arr-np.nanmean(arr))**2)))


def normint_pop(arr, pop):
    """Return normalized arr with a population array as reference
    """
    return (arr - np.nanmean(pop)) \
        / (np.sqrt(np.nansum((pop-np.nanmean(pop))**2)))


def numpy_fillna(data):
    """fill array with nan values such that all row have same length
    """
    # Get lengths of each row of data
    lens = np.array([len(i) for i in data])

    # Mask of valid places in each row
    mask = np.arange(lens.max()) < lens[:,None]

    # Setup output array and put elements from data into masked positions
    out = np.zeros(mask.shape, dtype=data.dtype)
    out[mask] = np.concatenate(data)
    return out

def nan_interpol(
    arr, axis=None, bounds_error=False, fill_value=0
):
    """Interpolate array around nan values
    """
    def nan_interpol_helper(arr, bounds_error, fill_value):
        nn_idx = np.where(~np.isnan(arr))[0]
        nan_idx = np.where(np.isnan(arr))[0]
        if len(nn_idx) < 2:
            warn("Only nan values")
            arr_interp = arr
        else:
            interp = interpolate.interp1d(
                nn_idx, arr[nn_idx],
                bounds_error=bounds_error,
                fill_value=fill_value
            )
            arr_interp = interp(np.arange(len(arr)))

        return arr_interp

    if axis is None:
        arr_interp = nan_interpol_helper(
            arr, bounds_error, fill_value)
    else:
        if axis == 1:
            arr_interp = []
            for iarr, ifill_value in zip(arr, fill_value):
                temp = nan_interpol_helper(
                    iarr, bounds_error, ifill_value)
                arr_interp.append(temp)
            arr_interp = np.array(arr_interp)
        else:
            arr_interp = np.apply_along_axis(
                nan_interpol_helper, axis, arr,
                bounds_error=bounds_error, fill_value=fill_value)
    return arr_interp


def arr_interp_resolve(arr, resolve=2, **kwargs_interp):
    """Extent resolution via interpolation
    """
    idx = list(range(len(arr)))*(resolve-1)
    arr_resolve = np.insert(arr, idx, np.nan)
    arr_interp = nan_interpol(
        arr_resolve, **kwargs_interp)
    return arr_interp


def cross(series, times, cross_thresh=0, direction='rising'):
    """
    Given a Series returns all the index values where the data values equal
    the 'cross' value.

    Direction can be 'rising' (for rising edge), 'falling' (for only falling
    edge), or 'cross' for both edges
    """
    # Find if values are above or bellow yvalue crossing:
    above = series > cross_thresh
    below = np.logical_not(above)
    left_shifted_above = above[1:]
    left_shifted_below = below[1:]
    x_crossings = []
    # Find indexes on left side of crossing point
    if direction == 'rising':
        idxs = (left_shifted_above & below[0:-1]).nonzero()[0]
    elif direction == 'falling':
        idxs = (left_shifted_below & above[0:-1]).nonzero()[0]
    else:
        rising = left_shifted_above & below[0:-1]
        falling = left_shifted_below & above[0:-1]
        idxs = (rising | falling).nonzero()[0]

    # Calculate x crossings with interpolation using formula for a line:
    # x_crossings = times[idxs]
    x1 = times[idxs]
    x2 = times[idxs+1]
    y1 = series[idxs]
    y2 = series[idxs+1]
    x_crossings = (cross_thresh-y1)*(x2-x1)/(y2-y1) + x1

    return x_crossings


def events_binary(
        series, times, direction='rising', **kwargs_gaussian_filter):
    """
    extract event times from a binary  signal (e.g. photodiode)
    """
    if kwargs_gaussian_filter:
        series = gaussian_filter(series, **kwargs_gaussian_filter)

    thresh = np.ptp(series)/2 + np.min(series)

    return cross(series, times, thresh, direction=direction)


def normalize_minmax(x, assert_bounds=True, return_normalizer=False):
    """
    Normalize an array so that its range is 2 and
    zero remains zero
    """

    if assert_bounds:
        assert (np.min(x) <= 0), "minimum must be smaller or equal to zero"
        assert (np.max(x) > 0), "maximum must be bigger than zero"

    maximum = np.nanmax(np.abs(x))

    norm_x = x / maximum

    if not assert_bounds:
        if np.min(norm_x) > 0:
            lower = 0
        else:
            lower = np.min(norm_x)
        if np.max(norm_x) <= 0:
            upper = 0
        else:
            upper = np.max(norm_x)

        if lower == 0 and upper == 0:
            raise ValueError('all values are zero')

        ptp = np.abs(upper - lower)
    else:
        ptp = np.ptp(norm_x)

    norm_x /= ptp * 0.5

    if return_normalizer:
        return norm_x, maximum * ptp * 0.5
    else:
        return norm_x


def align_center(
        arr, method='nanargmax',
        nan_padding=0, gaussian_filter=False, **gaussian_kwargs):
    """
    Align center to an index given by a method
    (e.g. align to maximum or minimum)
    """
    if nan_padding:
        arr = np.concatenate([
            np.empty(nan_padding)*np.nan,
            arr,
            np.empty(nan_padding)*np.nan
            ])

    if isinstance(method, str):
        method = getattr(np, method)

    if gaussian_filter:
        filtered_arr = nangaussian_filter(arr, **gaussian_kwargs)
        center_arg = method(filtered_arr)

    else:
        center_arg = method(arr)

    rollby = int(arr.shape[0]//2 - center_arg)

    #if rollby < 0:
    #    rollby += arr.shape[0]

    return np.roll(arr, rollby)
