import numpy as np
import os
from scipy.signal import medfilt
from scipy.interpolate import interp1d
from scipy.stats import mode
from scipy.ndimage.filters import gaussian_filter1d
import getpass

from . import TMP_FOLDER
from .. import numerical
# TODO make signal extraction class


def simple_dfof(
        movie, masks, timestamps, rate, roi_ids,
        bg_subtract=False,
        dfof_method='dfof_time',
        dfof_filter=None,
        filter_kwargs={},
        filter_before=False,
        nonzero=True,
        normalize_movie=False,
        dfof_bg=False,
        **dfof_kwargs
        ):
    """simple dfof calculation from movie given ROI masks.
    Prepares for insertion into axolotl database.
    """
    if normalize_movie:
        movie = (movie - np.nanmin(movie)) \
            / (np.nanmax(movie) - np.nanmin(movie))

    traces = mean_extraction(movie, masks, nonzero=nonzero)

    if bg_subtract:
        bg = ~masks.sum(0).astype(bool)
        bg_trace = mean_extraction(movie, bg[None, :], nonzero=nonzero)
        min_trace = np.min(traces, 0, keepdims=True)
        if dfof_bg:
            assert np.all(bg_trace > 0)
            traces = (traces - bg_trace)/bg_trace
        else:
            traces -= bg_trace
            traces -= np.min(traces, 0, keepdims=True) - min_trace

    if normalize_movie:
        assert np.all(1 >= traces) & np.all(traces >= 0), (
                "traces not between 0 and 1"
                )

    if filter_before:
        # apply a number of filters to dfof signal
        if dfof_filter is None:
            pass
        else:
            traces, timestamps, rate = filter_dfof(
                    traces, timestamps, rate, dfof_filter, **filter_kwargs)

    if dfof_method == 'dfof_time':
        traces = dfof_time(traces, timestamps, **dfof_kwargs)
    elif dfof_method == 'dfof_percentile':
        traces = dfof_percentile(traces, **dfof_kwargs)
    elif dfof_method == 'dfof_rolling':
        traces = dfof_rolling(traces, rate, **dfof_kwargs)
    elif dfof_method == 'dfof_lowpass':
        traces = dfof_lowpass(traces, rate, **dfof_kwargs)
    elif dfof_method == 'f_rolling':
        traces = f_rolling(traces, rate, **dfof_kwargs)

    # apply a number of filters to dfof signal
    if not filter_before:
        if dfof_filter is None:
            pass
        else:
            traces, timestamps, rate = filter_dfof(
                    traces, timestamps, rate, dfof_filter, **filter_kwargs)

    # format output for axolotl
    output = {}
    output['signal'] = traces
    output['timestamps'] = timestamps
    output['signal_unit'] = 'dF/F'
    output['time_unit'] = 'sec'
    output['signal_resolution'] = np.diff(traces, axis=0).min()
    output['signal_offset'] = timestamps[-1]
    output['rate'] = rate
    if isinstance(roi_ids, (float, int)):
        roi_ids = [roi_ids]
    output['roi_ids'] = list(roi_ids)
    return output

def interp_dfof(dfof, timestamps, interp_rate=25):
    """interpolate dfof
    """
    interp = interp1d(timestamps, dfof, axis=0, assume_sorted=True)
    timestamps = np.arange(timestamps[0], timestamps[-1], 1/interp_rate)
    dfof = interp(timestamps)
    return dfof, timestamps, interp_rate


def filter_dfof(
        dfof, timestamps, rate,
        method='median', ksize=0.24, interp_rate=None, **kwargs):
    """
    Interpolate and median filter dfof trace

    Parameters
    ----------
    dfof : numpy.ndarray
        dfof array with shape t x ROIs.
    timestamps : numpy.ndarray
        timestamps for dfof.
    rate : float
        rate of acquisition in Hz.
    ksize : float
        kernel size for median filter in seconds.
    interp_rate : float
        rate to interpolate to before applying filter in Hz. Set to
        None, if you want to skip this step

    Returns
    -------
    dfof : numpy.ndarray
    timestamps : numpy.ndarray
    rate : float
    """
    # interpolate data to different rate
    if interp_rate is not None:
        dfof, timestamps, rate = interp_dfof(dfof, timestamps, interp_rate)
    # apply filter
    if method == 'medfilt':
        dfof = np.apply_along_axis(
                medfilt, 0, dfof,
                int(numerical.round(rate * ksize, 'odd')), **kwargs)
    elif method == 'gaussian':
        dfof = gaussian_filter1d(dfof, axis=0, sigma=(rate * ksize), **kwargs)
    else:
        raise NameError(f'method {method} for filter dfof does not exist')

    return dfof, timestamps, rate


def mean_extraction(movie, masks, nonzero=True, ):
    """Return traces from masks (possibly weighted)
    by calculating the mean pixel value for each mask
    at each time point.
    """
    if masks.ndim == 2:
        masks = masks[None, ...]

    # movie: T x h x w - masks: ROI x h x w
    # trace: T x ROI
    A = movie[..., None] * np.moveaxis(masks, 0, -1)[None, ...]
    if nonzero:
        return numerical.nonzero_mean(A, (1, 2))
    else:
        return np.nanmean(A, (1, 2))


def dfof_percentile(traces, q=10):
    """Calculate dfof from percentile of trace
    """
    baseline = np.percentile(traces, axis=0, q=q, keepdims=True)
    return (traces - baseline)/baseline


def dfof_time(traces, timestamps, intervals, method='mean', **method_kwargs):
    """Calculate dfof from time intervals

    Parameters
    ----------
    traces : numpy.array
        T x ROI array.
    timestamps : numpy.array
        T array.
    intervals : tuple, list of tuples
        Intervals over which to apply the method to calculate the baseline
    method : str
        Numpy method to use for calculating the baseline
    method_kwargs : dict
        Arguments to pass to method.

    Returns
    -------
    dfof : numpy.array
        Calculated dfof traces (T x ROI).
    """
    if isinstance(intervals, tuple):
        intervals = [intervals]
    #
    duration = timestamps[-1]
    intervals = [
            (i, j)
            if i >= 0 and j >= 0
            else
            (i, duration + j)
            if i >= 0
            else
            (duration + i, j)
            if j >= 0
            else
            (duration + i, duration + j)
            for i, j in intervals
            ]

    # find timestamps within interval
    bool_intervals = np.logical_or.reduce([
        np.logical_and(timestamps >= i, timestamps < j)
        for i, j in intervals])

    # calculate baseline
    baseline = getattr(np, method)(
            traces[bool_intervals], axis=0, **method_kwargs)[None, :]

    # calculate dfof
    return (traces - baseline)/baseline


def dfof_rolling(traces, rate, window=30, method='nanmean', **kwargs):
    """
    compute dfof from rolling window
    """

    window = int(window*rate)

    rolled_traces = numerical.simple_rolling_window(
            traces, window, axis=0, border='edges').astype(float)

    baseline = getattr(np, method)(rolled_traces, axis=1, **kwargs)

    return (traces - baseline)/baseline


def modehist(x, bins, axis=None, **kwargs):
    """

    """

    if axis is None:

        pdf, edges = np.histogram(x, bins, **kwargs)

        medges = np.mean([edges[1:], edges[:-1]], axis=0)

        return medges[np.argmax(pdf)]

    else:

        return np.apply_along_axis(
                modehist, axis, x, bins, **kwargs
                )


def f_rolling(traces, rate, window=30, use_mode=False, bins=50):
    """
    """

    window = int(window*rate)

    if use_mode:
        traces_rounded = numerical.round(traces, method='sig', digits=1)
        min_trace = mode(traces_rounded, axis=0)[0]
    else:
        traces_rounded = traces
        arange = (np.min(traces), np.max(traces))
        min_trace = modehist(traces, bins, axis=0, range=arange)[None, ...]

    rolled_traces = numerical.simple_rolling_window(
            traces_rounded, window, axis=0, border='edges').astype(float)

    if use_mode:
        baseline = mode(rolled_traces, axis=1)[0][:, 0]
    else:
        baseline = modehist(rolled_traces, bins, range=arange, axis=1)

    traces = traces - baseline + min_trace
    # traces -= np.min(traces, 0, keepdims=True) - min_trace

    return traces
