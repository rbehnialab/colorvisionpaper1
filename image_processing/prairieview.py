"""functions for reading PrairieView imaging data
"""
from datetime import timedelta
from glob import glob
import os
import xml.etree.ElementTree as ET
import pandas as pd
import numpy as np
from warnings import warn
from PIL import Image
from joblib import Parallel, delayed

from . import TMP_FOLDER

REC_TYPES = ['TSeries', 'ZSeries', 'SingleImage']
V_NAMES = {
    'VoltageRecording':'Vin',
    'VoltageOutput':'Vout'
}
PV_POSITIONS = [
    'positionCurrentXAxis0',
    'positionCurrentYAxis0',
    'positionCurrentZAxis0'
]
PV_MPP = [
    'micronsPerPixelXAxis',
    'micronsPerPixelYAxis'
]

def fetch_tiffs(
        tiff_files, tiff_folder,
        timestamps, frame_period,
        raw_series_unit=None,
        raw_series_resolution=None,
        time_unit='sec',
        datapath=None,
        function='memmap_tiffs',
        **kwargs
        ):
    """Fetch data to insert into axolotl database
    """
    if datapath is not None:
        tiff_folder = os.path.join(datapath, tiff_folder)
    #
    if function == 'memmap_tiffs':
        tiff_map = memmap_tiffs(tiff_files, tiff_folder, **kwargs)
    else:
        raise NameError(f'function {function} not found')

    output = {}
    output['timestamps'] = timestamps
    output['raw_series_offset'] = timestamps[0]
    output['rate'] = 1/frame_period
    if raw_series_unit is None:
        output['raw_series_unit'] = str(tiff_map.dtype)
    else:
        output['raw_series_unit'] = raw_series_unit
    output['time_unit'] = time_unit
    if isinstance(raw_series_resolution, str):
        raw_series_resolution = eval(raw_series_resolution)
    if isinstance(raw_series_resolution, list):
        raw_series_resolution = raw_series_resolution[0]
    output['raw_series_resolution'] = raw_series_resolution
    output['raw_series'] = tiff_map
    return output


def memmap_tiffs(
        tiff_files, tiff_folder, n_jobs=None, parallel=None,
        use_tmp=False, memmap_file='tiff_memmap.dat'):
    """From single tiff files create a memmap numpy file.
    #TODO: Uses the hash convention from datajoint to name file.
    #TODO: parallelize
    """
    if not isinstance(tiff_files, list):
        tiff_files = list(tiff_files)
    #
    tiff_file1 = tiff_files[0]
    #
    im = np.array(Image.open(os.path.join(tiff_folder, tiff_file1)))
    shape = (len(tiff_files), *im.shape)
    dtype = im.dtype
    #
    if use_tmp:
        memmap_file = os.path.join(TMP_FOLDER, memmap_file)
    else:
        memmap_file = os.path.join(tiff_folder, memmap_file)
    tiff_map = np.memmap(memmap_file, dtype=dtype, mode='w+', shape=shape)
    tiff_map[0] = im
    #
    def assign_image(n, tiff_file, tiff_map):
        tiff_file = os.path.join(tiff_folder, tiff_file)
        im = np.array(Image.open(tiff_file))
        tiff_map[n+1] = im
    #
    if n_jobs is None and parallel is None:
        for n, tiff_file in enumerate(tiff_files[1:]):
            assign_image(n, tiff_file, tiff_map)
    else:
        if parallel is None:
            parallel = Parallel(n_jobs=n_jobs)
        parallel(
                delayed(assign_image)(n, tiff_file, tiff_map)
                for n, tiff_file in enumerate(tiff_files[1:])
                )
    tiff_map.flush()
    ###
    return tiff_map

def fetch_xml(datafolder, was_completed=True, **kwargs):
    """Fetch PrairieView xml data to insert into axolotl database

    Parameters
    ----------
    datafolder : str
    kwargs : dict
        Arguments passed to find_recording function
    """
    if not was_completed:
        return None
    output = {}
    recfolder = find_recording(datafolder, **kwargs)
    if recfolder is None:
        return None
    output['twophoton_dataname'] = recfolder
    #
    xmlpath = os.path.join(datafolder, recfolder, recfolder+'.xml')
    #
    pv_settings, sequences = read_xml(xmlpath)
    output['twophoton_metadata'] = pv_settings
    output['frames_metadata'] = sequences.to_records(False)
    ###
    output['timestamps'] = np.array(sequences['absoluteTime'])
    output['twophoton_tiffs'] = np.array(sequences['filename'])
    #
    output['pmt_gain0'] = pv_settings.get('pmtGain0')
    output['pmt_gain1'] = pv_settings.get('pmtGain1')
    output['laser_power'] = pv_settings.get('laserPower0')
    output['laser_wavelength'] = pv_settings.get('laserWavelength0')
    output['dwell_time'] = pv_settings.get('dwellTime')
    output['pixels_per_line'] = pv_settings.get('pixelsPerLine')
    output['lines_per_frame'] = pv_settings.get('linesPerFrame')
    output['scan_line_period'] = pv_settings.get('scanLinePeriod')
    output['frame_period'] = pv_settings.get('framePeriod')
    output['twophoton_position'] = [
        pv_settings.get(pos) for pos in PV_POSITIONS]
    output['microns_per_pixel'] = [
        pv_settings.get(mpp) for mpp in PV_MPP
    ]
    output['imaging_offset'] = sequences['absoluteTime'].iloc[0]
    output['absolute_time_vout'] = pv_settings.get('absoluteTimeVout')
    output['absolute_time_vin'] = pv_settings.get('absoluteTimeVin')
    if 'dataFileVin' in output['twophoton_metadata']:
        vin_csv = os.path.join(datafolder, recfolder, output['twophoton_metadata']['dataFileVin'])
        csvdata = pd.read_csv(vin_csv).rename(columns=lambda x: str(x).strip())
        output['vin_csv_data'] = csvdata.to_records(False)
    #output['twophoton_file_regex']
    return output

def find_recording(
    datafolder, recording_type, file_extension, date,
    days_before=0, date_format='%m%d%Y', ext_fill=3,
    file_format='{recording_type}-{date}-????-{file_extension}',
    errors='warn'
    ):
    """Find the correct PraireView folder corresponding
    to a recording.

    Parameters
    ----------
    datafolder : str
        Path to data storage folder
    recording_type : str
        Type of recording (TSeries, ZSeries, SingleImage)
    file_extension : str or int
        The extension to the file
    date : datetime
        date of the recording
    days_before : int
        The days before the date to look for the recording
    file_format : str
        Formating of recording folder.

    Returns
    -------
    recording_folder : str
        Name recording folder.
    """
    if not recording_type in REC_TYPES:
        raise Exception(f"Unknown recording type: {recording_type}.")
    check_dates = [
        (date - timedelta(days=d)).strftime(date_format)
        for d in range(days_before+1)
    ]
    matches_found = []
    for check_date in check_dates:
        matches_found.extend(
                glob(os.path.join(
                    datafolder, file_format.format(
                        recording_type=recording_type,
                        date=check_date,
                        file_extension=str(file_extension).zfill(ext_fill))
                )))
    if len(matches_found) == 0:
        if errors == 'warn':
            warn(f'Could not find folder matching {file_extension} on the {date}')
            return None
        elif errors == 'ignore':
            return None
        else:
            raise NameError(f'no match found for file extension {file_extension} on the {date}')
    elif len(matches_found) != 1:
        raise NameError(f'multiple matches found for recording: {matches_found}')
    else:
        return os.path.split(matches_found[0])[-1]


def read_xml(xmlpath):
    """Read the metadata from the prairie view xml file.
    Returns
    -------
    pv_settings : dictionary
        General settings of prairie view.
    sequences : pandas.DataFrame
        Dataframe of each frame's metadata
    """
    def shard_helper(stateshard):
        #check if right tag
        if stateshard.tag != 'PVStateShard':
            raise Exception(f"{stateshard.tag}")
        #initialize settings dictionary
        settings = {}
        #
        for statevalue in list(stateshard):
            if statevalue.tag != 'PVStateValue':
                raise Exception(f"{statevalue.tag}")
            elif len(statevalue) == 0:
                attrib = statevalue.attrib
                settings[attrib['key']] = attrib['value']
                continue
            statekey = statevalue.attrib['key']
            for indexvalue in list(statevalue):
                if indexvalue.tag == 'IndexedValue':
                    attrib = indexvalue.attrib
                    settings[statekey+attrib['index']] = attrib['value']
                    continue
                elif indexvalue.tag != 'SubindexedValues':
                    raise Exception(f"{indexvalue.tag}")
                indexkey = indexvalue.attrib['index']
                for subindexvalue in list(indexvalue):
                    if subindexvalue.tag != 'SubindexedValue':
                        raise Exception(f"{subindexvalue.tag}")
                    attrib = subindexvalue.attrib
                    settings[
                        statekey+indexkey+attrib['subindex']
                    ] = attrib['value']
        return settings
    ###
    tree = ET.parse(xmlpath)
    root = tree.getroot()
    date = root.attrib['date']
    date = pd.to_datetime(date).to_pydatetime()
    #
    if root[0].tag == 'PVStateShard':
        pv_settings = shard_helper(root[0])
    elif root[1].tag == 'PVStateShard' and root[0].tag == 'SystemIDs':
        pv_settings = shard_helper(root[1])
        pv_settings['SystemID'] = root[0].get('SystemID')
    else:
        raise Exception("Unknown root structure for xml")
    pv_settings['date'] = date
    sequences = pd.DataFrame()
    #
    for sequence in root.findall('Sequence'):
        #initialize frames
        frames = []
        seq_attrib = sequence.attrib
        #TODO check that if there are mutliple voltage inputs/outputs
        for vname, vshort in V_NAMES.items():
            #
            v = sequence.find(vname)
            if v is None:
                v_attrib = {}
            else:
                v_attrib = {k+vshort:v for k, v in v.items()}
            seq_attrib.update(v_attrib)
        ###
        for frame in sequence.findall('Frame'):
            f_attrib = frame.attrib
            for key in ['File', 'ExtraParameters']:
                f_attrib.update(frame.find(key).attrib)
            shard_dict = shard_helper(frame.find('PVStateShard'))
            f_attrib.update(shard_dict)
            frames.append(f_attrib)
        #make frames dataframe and add seq_attrib
        frames = pd.DataFrame(frames)
        for k, v in seq_attrib.items():
            frames[k] = v
        #
        sequences = sequences.append(frames, ignore_index=True)
    #add singly unique columns to pv_settings if not in
    #change string of integers and floats if possible
    for k, v in pv_settings.items():
        if isinstance(v, str):
            try:
                pv_settings[k] = int(v)
            except ValueError:
                try:
                    pv_settings[k] = float(v)
                except ValueError:
                    pass
    for column, series in sequences.iteritems():
        series = pd.to_numeric(series, errors='ignore')
        sequences[column] = series
        if column not in pv_settings:
            u = series.unique()
            if len(u) == 1:
                pv_settings[column] = u[0]
    ###
    return pv_settings, sequences
