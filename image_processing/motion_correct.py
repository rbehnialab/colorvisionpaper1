"""Motion correct functions
"""
import caiman as cm
import cv2
from numpy.lib.format import open_memmap
from . import TMP_FOLDER
from scipy.optimize import curve_fit
from scipy.ndimage import gaussian_filter1d
from sklearn.decomposition import IncrementalPCA
import numpy as np
from itertools import product
from warnings import warn
import os
import getpass


def debleach(mov):
    """ Debleach by fiting a model to the median intensity.
    Copied from caiman package.
    """
    if not isinstance(mov[0, 0, 0], np.float32):
        mov = np.asanyarray(mov, dtype=np.float32)

    t, _, _ = mov.shape
    x = np.arange(t)
    y = np.median(mov.reshape(t, -1), axis=1)

    def expf(x, a, b, c):
        return a * np.exp(-b * x) + c

    def linf(x, a, b):
        return a * x + b

    try:
        p0 = (y[0] - y[-1], 1e-6, y[-1])
        popt, _ = curve_fit(expf, x, y, p0=p0)
        y_fit = expf(x, *popt)
    except:
        p0 = ((y[-1] - y[0])/(x[-1] - x[0]), y[0])
        popt, _ = curve_fit(linf, x, y, p0=p0)
        y_fit = linf(x, *popt)

    norm = y_fit - np.median(y[:])
    for frame in range(t):
        mov[frame, :, :] = mov[frame, :, :] - norm[frame]

    return mov


def caiman_methods(
        raw_file, method='motion_correct_rigid',
        template=None, **kwargs
        ):
    """Apply a caiman motion correction function and return movie files
    """
    mc_instance = cm.motion_correction.MotionCorrect(
            raw_file,
            **kwargs
            )
    mc_instance = getattr(mc_instance, method)(template=template, save_movie=True)
    return mc_instance.fname_tot_rig


def _smooth_helper_function(movie, shape, smooth_sigma, rate, **smooth_kwargs):
        sigma = smooth_sigma * rate
        for xidx, yidx in product(range(shape[1]), range(shape[2])):
            # gaussian filter each pixel
            pixel = gaussian_filter1d(
                    movie[:, xidx, yidx], sigma=sigma, **smooth_kwargs).astype(np.float32)

            movie[:, xidx, yidx] = pixel


def simple_movie_correction(
        raw, timestamps, rate, drop=5, components_denoise=100, batch_denoise=1000,
        ksize=(5,5), kstdx=1, kstdy=1, mc_movie=True, blur_movie=False,
        remove_comps=0, batch_remove=1000, remove_method='subtract',
        remove_comps2=0, batch_remove2=1000, remove_method2='subtract',
        denoise=True, n_jobs=None, template_method='gaussian_blur', template_interval=None,
        smooth=False, smooth_sigma=0.1, smooth_kwargs={}, smooth_after=False,
        **mc_kwargs
        ):
    """
    Simple steps to remove motion artifacts and noise
    """
    #create temporary file for movie
    #uuid4 = str(uuid.uuid4()) + '.npy'
    movie_filename = f'{getpass.getuser()}_temp_movie.npy'
    temp_file = os.path.join(TMP_FOLDER, movie_filename)
    #if os.path.exists(temp_file):
    #    os.remove(temp_file)

    #drop a number of initial frames
    drop_frames = int(drop * rate)
    shape = (raw.shape[0]-drop_frames, raw.shape[1], raw.shape[2])
    timestamps = timestamps[drop_frames:]

    #write movie to temporary file and debleach it
    movie = open_memmap(temp_file,
            mode='w+', dtype=np.float32, shape=shape)
    movie[:] = raw[drop_frames:, :, :].astype(np.float32)

    #remove spatial PCs from movie
    if remove_comps:
        movie = remove_components(
                movie, components=remove_comps,
                batch_size=batch_remove,
                method=remove_method
                )

    movie = debleach(movie)

    if smooth and not smooth_after:
        _smooth_helper_function(movie, shape, smooth_sigma, rate, **smooth_kwargs)

    if blur_movie:
        movie = gaussian_blur_movie(movie, ksize=ksize, kstdy=kstdy, kstdx=kstdx)

    movie.flush()

    if template_interval is None:
        movie_template = movie
    else:
        tbool_template = (timestamps >= template_interval[0]) \
            & (timestamps < template_interval[1] )
        movie_template = movie[tbool_template]

    #create template
    if template_method == 'gaussian_blur':
        template = gaussian_blur_template(movie_template, ksize=ksize, kstdy=kstdy, kstdx=kstdx)
    elif template_method == 'average':
        template = movie_template.mean(0)
    else:
        raise NameError(f'{template_method} does not exist.')

    #motion correct movie
    if mc_movie:
        if n_jobs is not None:
            try:
                _, dview, _ = cm.cluster.setup_cluster(backend='multiprocessing', n_processes=n_jobs)
                mc_kwargs['dview'] = dview
                created_cluster = True
            except:
                created_cluster = False
                warn("Could not create cluster")
        else:
            created_cluster = False
        mc_filename = caiman_methods(movie.filename, template=template, **mc_kwargs)
        if created_cluster:
            dview.terminate()
        m = cm.load(mc_filename)
    else:
        raise NotImplementedError('skipping motion correction')

    if smooth and smooth_after:
        _smooth_helper_function(movie, shape, smooth_sigma, rate, **smooth_kwargs)

    #denoise and calculate dF/F
    m = m.astype(np.float32)
    if denoise:
        m = m.IPCA_denoise(components=components_denoise, batch=batch_denoise)
    #m, _ = (m - m.min() + 1).computeDFF(secWindow=secWindow, method='delta_f_over_f')

    #write corrections to the temporary file
    movie[:] = m[:]

    #remove spatial PCs from movie
    if remove_comps2:
        movie = remove_components(
                movie, components=remove_comps2,
                batch_size=batch_remove2,
                method=remove_method2
                )
    ##
    movie.flush()
    ###delete motion correction files
    for filename in mc_filename:
        os.remove(filename)

    return movie, timestamps

def remove_components(movie, components=1, batch_size=1000, method='subtract', copy=False):
    """remove spatial components from movie
    """
    pca = IncrementalPCA(n_components=components, batch_size=1000)

    #shape
    T, d1, d2 = movie.shape

    #fit
    frames = movie.reshape(T, d1*d2)
    pca.fit(frames)

    #reduced
    reduced_movie = pca.inverse_transform(pca.transform(frames)).reshape(T, d1, d2)
    #
    if method == 'divide':
        if np.all(reduced_movie > 0):
            if copy:
                movie = movie/reduced_movie
            else:
                movie[:] = movie/reduced_movie
        else:
            raise Exception('if divide reduced movie must always be positive')
    elif method == 'subtract':
        if copy:
            movie = movie - reduced_movie
        else:
            movie[:] = (movie - reduced_movie)
    else:
        raise Exception("Can only use 'subtract', 'divide' as methods")
    return movie

def format_correction(
        raw, timestamps, rate,
        function='simple_movie_correction',
        cut_kwargs=None, **kwargs
        ):
    """format for axolotl entry
    """
    if cut_kwargs is not None:
        raw = cut_movie(raw, **cut_kwargs)

    if function == 'simple_movie_correction':
        movie, timestamps = simple_movie_correction(raw, timestamps, rate, **kwargs)
    else:
        raise Exception(f'function {function} not implemented')
    #
    output = {}
    output['series'] = movie
    output['timestamps'] = timestamps
    output['series_unit'] = str(movie.dtype)
    output['time_unit'] = 'sec'
    output['series_offset'] = timestamps[0]
    output['rate'] = rate
    return output

def gaussian_blur_movie(
        movie,
        ksize=(5,5), kstdx=1, kstdy=1,
        border_type=cv2.BORDER_REPLICATE
        ):
    """create template from numpy memmap using an averaging method
    and applying a gaussian blur
    """
    for idx, frame in enumerate(movie):
        movie[idx] =  cv2.GaussianBlur(frame, ksize=ksize, sigmaX=kstdx, sigmaY=kstdy, borderType=border_type)
    return movie

def gaussian_blur_template(
        arr, method='mean', axis=0,
        ksize=(5,5), kstdx=1, kstdy=1,
        border_type=cv2.BORDER_REPLICATE
        ):
    """create template from numpy memmap using an averaging method
    and applying a gaussian blur
    """
    template = getattr(arr, method)(axis=axis)
    if not template.ndim == 2:
        raise Exception('template must be two dimensional')
    return cv2.GaussianBlur(template, ksize=ksize, sigmaX=kstdx, sigmaY=kstdy, borderType=border_type)

def cut_movie(movie, top=0, bottom=0, left=0, right=0):
    """
    cut x pixels from movie

    Parameters
    ----------
    movie : T x h x w
    """
    return movie[
            :,
            top:movie.shape[1]-bottom,
            left:movie.shape[2]-right]
