"""functions for roi detection
"""
import cv2
import numpy as np
import pandas as pd
from scipy.ndimage.measurements import center_of_mass as com
from caiman import summary_images as si

from .. import numerical

# TODO make ROI detection class


def calculate_coms(masks):
    """calculate center of mass from multiple ROI masks
    """
    return np.array([com(m) for m in masks])


def simple_watershed(
        movie, proj_method='std', gaussian_blur=False,
        ksize=(5, 5), kstdx=1, kstdy=1, zscore=False,
        local_correlations_kwargs={},
        **watershed_kwargs):
    """apply watershed_rois and prepare for insertion into axolotl database
    """
    # project and pass to watershed_rois
    movie = (
            (movie - np.min(movie))
            / (np.max(movie) - np.min(movie)) * 255
            ).astype(np.uint8)
    if proj_method == 'local_correlations':
        proj = local_correlations(movie, **local_correlations_kwargs)
    else:
        proj = getattr(movie, proj_method)(0)

    if zscore:
        proj = numerical.zscore(proj)

    if gaussian_blur:
        proj = cv2.GaussianBlur(
                proj,
                ksize=ksize, sigmaX=kstdx,
                sigmaY=kstdy,
                borderType=cv2.BORDER_REPLICATE
                )

    masks = watershed_rois(proj, **watershed_kwargs)
    # return None if no ROIs were found
    if masks is None:
        print("No ROIs to insert")
        return None

    mcoms = calculate_coms(masks)
    sizes = masks.sum((1, 2))
    weight_masks = numerical.scale(
            masks * proj[None, :], axis=(1, 2), has_nan=False)
    im_shape = masks.shape[1:]

    # format
    output = {}
    output['roi_id'] = list(range(len(sizes)))
    output['mask'] = list(masks)
    output['weight_mask'] = list(weight_masks)
    output['size'] = list(sizes)
    output['com'] = list(mcoms)
    output = pd.DataFrame(output)
    output['im_shape'] = [im_shape] * len(sizes)
    output['label'] = 'watershedROI'
    return output


def watershed_rois(
        proj, thresh=2, upper_thresh=2.5,
        lower_thresh=1.5, min_size=9, sign=-1, edge_limit=0):
    """Detect ROIs using watershed.
    Works well for largely separated ROIs and clearly identifiable
    via a threshold.

    Parameters
    ----------
    proj : numpy.array
        projection of movie.
    thresh : float
        threshold for projection for active parts.
    upper_thresh : float
        sure foreground of projection
    lower_thresh : float
        unknown parts of projection
    min_size : int
        mininum size of ROI in pixels.
    sign : {-1, 1}
        If -1 the thresholded images are flipped.
    edge_limit : int
        Subtract this number from the height and width of the mask
        before thresholding the size of the ROI (min_size).
    Returns
    -------
    masks : numpy.array
        masks of each ROI (ROIs * y * x)
    """
    # height and width of projectiondd
    height, width = proj.shape

    # threshold image
    image = sign * numerical.binary_threshold(proj, thresh)
    image = np.uint8(image)

    # sure foreground
    fg = sign * numerical.binary_threshold(proj, upper_thresh)
    fg = np.uint8(fg)

    # unknown
    unknown = sign * numerical.binary_threshold(proj, lower_thresh)
    unknown = np.uint8(unknown)

    # get connected components
    ret, markers = cv2.connectedComponents(fg)
    markers += 1
    markers[unknown == 1] = 0

    # apply watershed
    image = cv2.cvtColor(image, cv2.COLOR_GRAY2BGR)
    markers = cv2.watershed(image, markers)
    markers[markers == 1] = 0

    # print number of rois
    unique = np.unique(markers)
    print(
            f"Found {len(unique) - 2} ROIs using "
            "watershed before size thresholding.")

    # create array of masks
    masks = np.array([
        markers == u
        for u in unique
        if (u > 0) and
        # limiting the edge and size thresholding
        (
            np.sum((markers == u)[
                edge_limit:height-edge_limit,
                edge_limit:width-edge_limit
            ]) > min_size)
        ])

    print(f"{len(masks)} ROIs left.")
    if len(masks) == 0:
        print(f"no ROIs found, returning None")
        return None
    return masks


def local_correlations(
        movie, eight_neighbours=False,
        swap_dim=False, frames_per_chunk=1500,
        order_mean=1, statistic='nanmax', **kwargs
        ):
    """
    Computes the correlation image for the input dataset movie
    Adaptaed from caiman package

    Parameters:
    -----------

    movie:  np.ndarray (3D or 4D)
        Input movie data in 3D or 4D format

    eight_neighbours: Boolean
        Use 8 neighbors if true, and 4 if false for 3D data (default = True)
        Use 6 neighbors for 4D data, irrespectively

    swap_dim: Boolean
        True indicates that time is listed in the
        last axis of Y (matlab format)
        and moves it in the front

    Returns:
    --------

    rho: d1 x d2 [x d3] matrix, cross-correlation with adjacent pixels

    """
    T = movie.shape[0]

    n_chunks = np.max([T // frames_per_chunk, 1])

    Cn = np.zeros((n_chunks, *movie.shape[1:]))

    for idx in np.arange(n_chunks):
        rho = si.local_correlations(
            np.array(
                movie[
                    idx * frames_per_chunk:
                    (idx + 1) * frames_per_chunk]
                ),
            eight_neighbours=eight_neighbours,
            swap_dim=swap_dim, order_mean=order_mean)

        Cn[idx] = rho

    rho = getattr(np, statistic)(Cn, axis=0, **kwargs)
    return rho
